class Driver < ActiveRecord::Base 
  has_many :travel_drivers, :dependent => :destroy
  has_many :monthlies, :dependent => :destroy


  attr_accessible :deleted, :name, :travel_drivers_attributes, :monthlies_attributes


  validates :name, presence: true




end
