class Space < ActiveRecord::Base
  belongs_to :location_type
  has_many :alocation_permanents, :dependent => :destroy
  has_many :reserve_of_physical_spaces, :dependent => :destroy
  attr_accessible :deleted, :capacity, :name, :observations, :price_local, :size, :location_type_id, :alocation_permanents_attributes, :reserve_of_physical_spaces_attributes

  validates :name, presence: true
  validates :capacity, presence: true
  validates :size, presence: true
  validates :price_local, presence: true

  accepts_nested_attributes_for :alocation_permanents
  
end
