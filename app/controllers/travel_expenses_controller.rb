class TravelExpensesController < ApplicationController
  
  def index
    @travel_expenses = TravelExpense.all
  end

  def show
    @travel_expense = TravelExpense.find(params[:id])
  end

  def new
    @travel_expense = TravelExpense.new
  end

  def edit
    @travel_expense = TravelExpense.find(params[:id])
  end

  def create
    @travel_expense = TravelExpense.new(params[:travel_expense])

    if @travel_expense.save
      redirect_to travel_expenses_path
    else
      render :new
    end
  end

  def update
    @travel_expense = TravelExpense.find(params[:id])

    if @travel_expense.update_attributes(params[:travel_expense])
      redirect_to travel_expenses_path
    else
      render :edit
    end
  end

  def destroy
    @travel_expense = TravelExpense.find(params[:id])
    @travel_expense.update_attribute(:deleted, true)
    redirect_to travel_expenses_path    
  end
end
