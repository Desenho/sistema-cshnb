require 'test_helper'

class RecessesControllerTest < ActionController::TestCase
  setup do
    @recess = recesses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:recesses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create recess" do
    assert_difference('Recess.count') do
      post :create, recess: { begin: @recess.begin, description: @recess.description, end: @recess.end }
    end

    assert_redirected_to recess_path(assigns(:recess))
  end

  test "should show recess" do
    get :show, id: @recess
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @recess
    assert_response :success
  end

  test "should update recess" do
    put :update, id: @recess, recess: { begin: @recess.begin, description: @recess.description, end: @recess.end }
    assert_redirected_to recess_path(assigns(:recess))
  end

  test "should destroy recess" do
    assert_difference('Recess.count', -1) do
      delete :destroy, id: @recess
    end

    assert_redirected_to recesses_path
  end
end
