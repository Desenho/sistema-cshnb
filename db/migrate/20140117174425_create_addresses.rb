class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :country
      t.string :state
      t.string :city
      t.string :district
      t.string :street
      t.string :numberzip_code
      t.string :complement
      t.references :user

      t.timestamps
    end
    add_index :addresses, :user_id
  end
end
