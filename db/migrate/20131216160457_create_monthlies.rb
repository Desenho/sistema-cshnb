class CreateMonthlies < ActiveRecord::Migration
  def change
    create_table :monthlies do |t|
      t.date :data
      t.integer :qtde
      t.boolean :transfer
      t.references :driver
      t.references :travel
      t.boolean :deleted

      t.timestamps
    end
    add_index :monthlies, :driver_id
    add_index :monthlies, :travel_id
  end
end
