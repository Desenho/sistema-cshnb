class CreateDrivers < ActiveRecord::Migration
  def change
    create_table :drivers do |t|
      t.string :name
      t.boolean :deleted

      t.timestamps
    end
  end
end
