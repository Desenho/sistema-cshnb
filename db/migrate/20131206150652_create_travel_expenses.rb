class CreateTravelExpenses < ActiveRecord::Migration
  def change
    create_table :travel_expenses do |t|
      t.float :price
      t.text :justification
      t.references :travel
      t.boolean :deleted

      t.timestamps
    end
    add_index :travel_expenses, :travel_id
  end
end
